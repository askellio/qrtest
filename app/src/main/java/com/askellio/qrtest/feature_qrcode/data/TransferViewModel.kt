package com.askellio.qrtest.feature_qrcode.data

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

class TransferViewModel: ViewModel() {
    var qrCodeResult: MutableLiveData<QrCodeResult>

    init {
        qrCodeResult = MutableLiveData()
    }
}