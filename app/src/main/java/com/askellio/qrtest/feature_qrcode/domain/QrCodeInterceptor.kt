package com.askellio.qrtest.feature_qrcode.domain

import com.askellio.qrtest.feature_qrcode.data.QrCodeResult
import com.google.zxing.Result

interface QrCodeInterceptor {
    fun process (result: Result): QrCodeResult
}