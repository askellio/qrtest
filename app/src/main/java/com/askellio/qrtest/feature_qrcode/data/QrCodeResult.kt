package com.askellio.qrtest.feature_qrcode.data

sealed class QrCodeResult

data class Success (val text: String): QrCodeResult()
data class Error (val resId: Int): QrCodeResult()
