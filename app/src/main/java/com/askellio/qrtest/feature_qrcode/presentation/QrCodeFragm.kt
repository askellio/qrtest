package com.askellio.qrtest.feature_qrcode.presentation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.askellio.qrtest.R
import com.askellio.qrtest.feature_qrcode.data.TransferViewModel
import com.askellio.qrtest.feature_result.ResultFragm
import kotlinx.android.synthetic.main.activity_main.view.*
import me.dm7.barcodescanner.zxing.ZXingScannerView

class QrCodeFragm: Fragment() {

    private lateinit var mScanner: ZXingScannerView
    private lateinit var mModel: QrCodeViewModel
    private lateinit var mTransferModel: TransferViewModel

    companion object {
        fun newInstance () = QrCodeFragm()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mModel = ViewModelProviders.of(this).get(QrCodeViewModel::class.java)
        mTransferModel = ViewModelProviders.of(activity!!).get(TransferViewModel::class.java)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        mScanner = ZXingScannerView(context)
        return mScanner
    }

    override fun onResume() {
        super.onResume()
        mScanner.startCamera()
        mScanner.setResultHandler {
            mModel.onResult(it)
        }
    }

    override fun onPause() {
        mScanner.setResultHandler (null)
        mScanner.stopCamera()
        super.onPause()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mModel.qrCodeResult.observe (viewLifecycleOwner, Observer {
            mTransferModel.qrCodeResult.value = it
            fragmentManager?.run {
                beginTransaction()
                    .replace(R.id.container_main, ResultFragm.newInstance())
                    .commit()
            }
        })
    }
}