package com.askellio.qrtest.feature_qrcode.domain

import com.askellio.qrtest.R
import com.askellio.qrtest.feature_qrcode.data.Error
import com.askellio.qrtest.feature_qrcode.data.QrCodeResult
import com.askellio.qrtest.feature_qrcode.data.Success
import com.google.zxing.BarcodeFormat
import com.google.zxing.Result

class QrCodeInterceptorImpl: QrCodeInterceptor {

    private fun isAllowedFormat (format: BarcodeFormat): Boolean = true

    override fun process(result: Result): QrCodeResult {

        return if (isAllowedFormat(result.barcodeFormat)) {
            val str = result.text
            if (str.isEmpty())
                Error(R.string.error_empty_code)
            else
                Success(str)
        }
        else
            Error(R.string.error_incorrect_format)
    }
}