package com.askellio.qrtest.feature_qrcode.presentation

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.askellio.qrtest.core.SingleLiveEvent
import com.askellio.qrtest.feature_qrcode.data.QrCodeResult
import com.askellio.qrtest.feature_qrcode.domain.QrCodeInterceptor
import com.google.zxing.Result
import org.koin.core.KoinComponent
import org.koin.core.inject

class QrCodeViewModel : ViewModel(), KoinComponent {

    val interceptor: QrCodeInterceptor by inject()

    val qrCodeResult: SingleLiveEvent<QrCodeResult>

    init {
        qrCodeResult = SingleLiveEvent()
    }

    override fun onCleared() {
        super.onCleared()
    }

    fun onResult (result: Result) {
        qrCodeResult.value = interceptor.process(result)
    }
}
