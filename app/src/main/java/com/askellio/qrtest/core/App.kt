package com.askellio.qrtest.core

import android.app.Application
import com.askellio.qrtest.feature_qrcode.domain.QrCodeInterceptor
import com.askellio.qrtest.feature_qrcode.domain.QrCodeInterceptorImpl
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.core.context.startKoin
import org.koin.dsl.module

class App: Application() {

    val qrCodeModule = module {
        single<QrCodeInterceptor> { QrCodeInterceptorImpl() }
    }

    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidLogger()
            androidContext(this@App)
            modules(qrCodeModule)
        }
    }
}