package com.askellio.qrtest.core

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.askellio.qrtest.R
import com.askellio.qrtest.feature_qrcode.presentation.QrCodeFragm
import com.askellio.qrtest.feature_result.ResultFragm

class SingleActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        if (savedInstanceState == null)
            supportFragmentManager.beginTransaction()
                .replace (R.id.container_main, QrCodeFragm.newInstance())
                .commit()
    }
}
