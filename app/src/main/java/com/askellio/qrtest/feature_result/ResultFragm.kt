package com.askellio.qrtest.feature_result

import android.content.res.ColorStateList
import android.content.res.Resources
import android.graphics.PorterDuff
import android.graphics.drawable.DrawableWrapper
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.content.res.AppCompatResources.getDrawable
import androidx.core.graphics.drawable.DrawableCompat
import androidx.fragment.app.Fragment
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProviders
import com.askellio.qrtest.R
import com.askellio.qrtest.feature_qrcode.data.Error
import com.askellio.qrtest.feature_qrcode.data.Success
import com.askellio.qrtest.feature_qrcode.data.TransferViewModel
import kotlinx.android.synthetic.main.fragm_result.*

class ResultFragm: Fragment() {

    private lateinit var mModel: ResultViewModel
    private lateinit var mTransferModel: TransferViewModel

    companion object {
        fun newInstance () = ResultFragm()
    }

    override fun onCreateView (
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?): View? {

        return inflater.inflate (
            R.layout.fragm_result,
            container,
            false
        )
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        mModel = ViewModelProviders.of(this).get(ResultViewModel::class.java)
        mTransferModel = ViewModelProviders.of(activity!!).get(TransferViewModel::class.java)
    }

    override fun onViewCreated(
        view: View,
        savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        mTransferModel.qrCodeResult.observe(viewLifecycleOwner, Observer {
            Log.d("tagx", "observed")
            val iconDrawable: Int
            val iconColor: Int
            val text: String
            when (it) {
                is Success -> {
                    text = it.text
                    iconColor = R.color.result_success
                    iconDrawable = R.drawable.ic_result_success
                }
                is Error -> {
                    text = getString(it.resId)
                    iconColor = R.color.result_error
                    iconDrawable = R.drawable.ic_result_error
                }
            }

            Log.d("tagx", text)
            result_text.text = text
            val icon = DrawableCompat.wrap (
                getDrawable (context!!, iconDrawable)!!
            )
            DrawableCompat.setTint (icon, iconColor)
            DrawableCompat.setTintMode (icon, PorterDuff.Mode.SRC_IN)
            result_icon.setImageDrawable (icon)
        })
    }
}